<?php

abstract class Hewan
{

    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;

    public function __construct($nama)
    {
        $this->nama = $nama;
    }

    abstract public function atraksi();
}

abstract class Fight extends Hewan
{
    public $attackPower;
    public $defencePower;
    public $musuh;

    abstract public function diserang($musuh);

    abstract public function serang($musuh);
}

class Elang extends Fight
{
    public $jenis = "Elang";
    public $nama;
    public $darah = 50;
    public $jumlahKaki = 2;
    public $keahlian = "terbang tinggi";
    public $attackPower = 10;
    public $defencePower = 5;

    public $musuh;
    public $this;

    public function atraksi()
    {
        return $this->nama . " sedang " . $this->keahlian;
    }

    public function getInfoHewan()
    {
        echo "
            Jenis Hewan = " . $this->jenis . "<br>
            Jumlah Kaki = " . $this->jumlahKaki . "<br>
            Darah = " . $this->darah . "<br>
            Keahlian = " . $this->keahlian . "<br>
            Attack Power = " . $this->attackPower . "<br>
            Defence Power = " . $this->defencePower . "<br>
        ";
    }

    public function diserang($musuh)
    {
        echo  $musuh->nama . " diserang " . $this->nama;
        $musuh->darah = $musuh->darah - ($this->attackPower / $musuh->defencePower);
    }

    public function serang($musuh)
    {
        $this->getInfoHewan();
        echo  "<br>";
        $musuh->getInfoHewan();
        echo  "<br>";
        echo  $this->nama . " menyerang " . $musuh->nama  . "<br>";
        $this->diserang($musuh);
        echo  "Darah " . $this->nama . " = " . $this->darah . "<br>";
        echo  "Darah " . $musuh->nama . " = " . $musuh->darah . "<br>";
    }
}

class Harimau extends Fight
{
    public $jenis = "Harimau";
    public $nama;
    public $darah = 50;
    public $jumlahKaki = 4;
    public $keahlian = "lari cepat";
    public $attackPower = 7;
    public $defencePower = 8;

    public function atraksi()
    {
        return $this->nama . " sedang " . $this->keahlian;
    }


    public function getInfoHewan()
    {
        echo "
            Jenis Hewan = " . $this->jenis . "<br>
            Jumlah Kaki = " . $this->jumlahKaki . "<br>
            Darah = " . $this->darah . "<br>
            Keahlian = " . $this->keahlian . "<br>
            Attack Power = " . $this->attackPower . "<br>
            Defence Power = " . $this->defencePower . "<br>
        ";
    }


    public function diserang($musuh)
    {
        echo  $musuh->nama . " diserang " . $this->nama;
        $musuh->darah = $musuh->darah - ($this->attackPower / $musuh->defencePower);
    }

    public function serang($musuh)
    {
        $this->getInfoHewan();
        echo  "<br>";
        $musuh->getInfoHewan();
        echo  "<br>";
        echo  $this->nama . " menyerang " . $musuh->nama . "<br>";
        $this->diserang($musuh);
        echo  "Darah " . $this->nama . " = " . $this->darah . "<br>";
        echo  "Darah " . $musuh->nama . " = " . $musuh->darah . "<br>";
    }
}

$elang_belang = new Elang("elang pak rt");

$harimau_belang = new Harimau("harimau pak rt");

// $harimau_belang->serang($elang_belang);
echo $harimau_belang->serang($elang_belang);
