<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Mail\PostAuthorMail;
use Illuminate\Http\Request;
use App\Events\CommentStored;
use App\Mail\CommentAuthorMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth:api')->except(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $post_id = $request->post_id;
        $comments = Comment::where('post_id', $post_id)->latest()->get();

        if ($comments) {
            return response()->json([
                'success'   => true,
                'message'   => 'Data daftar comment per post berhasil ditampilkan',
                'data'      => $comments,
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message'   => 'Data comment dengan post id : ' . $post_id . ' tidak ditemukan',
        ], 404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'content'         => 'required',
            'post_id'         => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $comment = Comment::create([
            'content'       => $request->content,
            'post_id'       => $request->post_id,
        ]);

        //memanggil event CommentStored
        event(new CommentStored($comment));


        // // mail ke pemilik post
        // Mail::to($comment->post->user->email)->send(new PostAuthorMail($comment));
        // // mail ke pemilik komentar
        // Mail::to($comment->user->email)->send(new CommentAuthorMail($comment));

        if ($comment) {

            return response()->json([
                'success'   => true,
                'message'   => 'Data comment berhasil dibuat',
                'data'      => $comment,
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message'   => 'Data comment gagal dibuat',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comment = Comment::find($id);

        if ($comment) {

            $user = auth()->user();
            if ($comment->user_id != $user->id) {
                return response()->json([
                    'success'       => false,
                    'message'       => 'Comment bukan milik user',
                ], 403);
            }
            return response()->json([
                'success'       => true,
                'message'       => 'Data comment berhasil ditampilkan',
                'data'          => $comment
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message'   => 'Data dengan id : ' . $id . ' tidak ditemukan',
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allRequest = $request->all();
        $validator = Validator::make($allRequest, [
            'content'           => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $comment = Comment::find($id);

        if ($comment) {

            $user = auth()->user();
            if ($comment->user_id != $user->id) {
                return response()->json([
                    'success'       => false,
                    'message'       => 'Comment bukan milik user',
                ], 403);
            }
            $comment->update([
                'content'         => $request->content,
            ]);

            return response()->json([
                'success'       => true,
                'message'       => 'Data post dengan id : ' . $id . ' berhasil diubah',
                'data'          => $comment
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message'   => 'Data dengan id : ' . $id . ' tidak ditemukan',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = Comment::find($id);

        if ($comment) {

            $comment->delete();
            return response()->json([
                'success'       => true,
                'message'       => 'Data comment berhasil dihapus',
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message'   => 'Data dengan id : ' . $id . ' tidak ditemukan',
        ], 404);
    }
}
